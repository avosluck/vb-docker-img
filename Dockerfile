# Устанавливаем базовый образ
FROM ubuntu:latest

# Обновление списка пакетов
RUN apt-get update

# Установка необходимых пакетов
RUN apt-get install -y curl unzip sudo

# Устанавливаем версии Terraform и AWS CLI
ARG TERRAFORM_VERSION="1.4.6"
ARG AWSCLI_VERSION="2.2.39"

# Установка AWS CLI
RUN curl "https://d1vvhvl2y92vvt.cloudfront.net/awscli-exe-linux-x86_64-${AWSCLI_VERSION}.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    sudo ./aws/install

# Установка Terraform
RUN curl -LO https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin

# Убираем лишние файлы
RUN rm -f awscliv2.zip terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# Проверяем версии
RUN aws --version && terraform -v
